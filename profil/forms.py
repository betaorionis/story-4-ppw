from django import forms
from profil import models

class MatkulForm (forms.Form):
    nama = forms.CharField(
        label = 'Nama Matkul',
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "PPW"}),
    )

    kelas = forms.CharField(
        label = 'Kelas',
        max_length = 50,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "2306"}),
    )

    dosen = forms.CharField(
        label = 'Nama Dosen',
        max_length = 50,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Nama Dosen"}),
    )

    sks = forms.CharField(
        label = 'Jumlah SKS',
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "3"}),
    )

    smt = forms.ChoiceField(
        label = 'Semester',
        choices=[('Genap', 'Genap'),
                ('Ganjil', 'Ganjil')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'})
    )
    
    tahun = forms.ChoiceField(
        label = 'Semester',
        choices = [
                ('2019/2020', '2019/2020'),
                ('2020/2021', '2020/2021'),
                ('2021/2022', '2021/2022'),
                ('2022/2023', '2022/2023'),
                ('2023/2024', '2023/2024'),
                ],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'}),
    )

class Meta:
    model = models.Matkul
    fields = ('nama', 'kelas', 'dosen', 'sks', 'smt',)