from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    kelas = models.CharField(max_length=50)
    dosen = models.CharField( max_length=50)
    sks = models.CharField(max_length=50)
    smt = models.CharField(max_length=50)
    thn = models.CharField(max_length=50)

