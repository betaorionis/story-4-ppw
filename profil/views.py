from django.shortcuts import render
from django.shortcuts import render, redirect
from profil import forms, models
from django.http import HttpResponseRedirect
from datetime import datetime
from .models import Matkul

# Create your views here.
def index(request):
    return render(request, "story3.html", {})

def story1(request):
    return render(request, "story1.html", {})

def games(request):
    return render(request, "games.html", {})

def time(request, additional = 0):
    now = datetime.datetime.now()
    hour = (now.hour + additional) % 24
    curtime = now.replace(hour=hour).strftime("%H:%M:%S")
    
    return render(request, "currenttime.html", {"time":curtime})

def matkul(request):
    if request.method == "POST":
        form = forms.MatkulForm(request.POST)

        if 'id' in request.POST:
            Matkul.objects.get(id=request.POST['id']).delete()

            return redirect('/matkul')

        if form.is_valid():
            matkul = models.Matkul(
                nama = form.data['nama'],
                kelas = form.data['kelas'],
                dosen = form.data['dosen'],
                sks = form.data['sks'],
                smt = form.data['smt'] + " " + form.data['tahun'],
                )
            matkul.save()

            return redirect('/matkul')

    else:
        form = forms.MatkulForm()

    context = {
        'context' : {'now': datetime.now()},
        'matkul': models.Matkul.objects.all().values(),
        'form' : form,
        }

    return render(request, 'matkul.html', context)

# def details(request, id):
#     for i in 
#     matkul.id = "/matkul/" + matkul.id

#     return render(request, "details.html", matkul_details)
